﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoCheckers.Models;
using NUnit.Framework;
using System.Drawing;
using System.Windows.Forms;

namespace AutoCheckers
{
    [TestFixture]
    class Tests
    {
        [Test]
        public void ReturnCloneMap()
        {
            var mapCells = new MapCell[2,2];
            for (var i = 0; i < mapCells.GetLength(0); i++)
                for (var j = 0; j < mapCells.GetLength(1); j++)
                {
                    mapCells[i, j] = new MapCell(new Point(i, j));
                }
            var aa = MapCell.CloneMap(mapCells);
            Assert.AreEqual(mapCells, aa);
        }
        [Test]
        public void ColorChanged()
        {
            var mapCells = new MapCell[2, 2];
            for (var i = 0; i < mapCells.GetLength(0); i++)
                for (var j = 0; j < mapCells.GetLength(1); j++)
                {
                    mapCells[i, j] = new MapCell(new Point(i, j));
                    mapCells[i, j].Picture = new PictureBox()
                    {
                        BackColor = Color.AntiqueWhite
                    };

                }
            GameState gameState = new GameState();
            gameState.InitCounter();
            gameState.mapCells = mapCells;

            gameState.mapCells[1, 1].UpdateCounter();
            gameState.mapCells[1, 1].Warrior = gameState.counterWarriors[gameState.mapCells[1, 1].CounterWarrior];            
            gameState.mapCells[1, 1].Picture.BackColor = gameState.mapCells[1, 1].Warrior == null ? Color.AntiqueWhite : gameState.mapCells[1, 1].Warrior.Color;

            Assert.AreEqual(gameState.mapCells[1, 1].Picture.BackColor, gameState.mapCells[1, 1].Warrior.Color);
        }

        [Test]
        public void WarriorChanged()
        {
            var mapCells = new MapCell[2, 2];
            for (var i = 0; i < mapCells.GetLength(0); i++)
                for (var j = 0; j < mapCells.GetLength(1); j++)
                {
                    mapCells[i, j] = new MapCell(new Point(i, j));
                    mapCells[i, j].Picture = new PictureBox()
                    {
                        BackColor = Color.AntiqueWhite
                    };

                }
            GameState gameState = new GameState();
            gameState.InitCounter();
            gameState.mapCells = mapCells;

            gameState.mapCells[1, 1].UpdateCounter();
            gameState.mapCells[1, 1].UpdateCounter();
            gameState.mapCells[1, 1].Warrior = gameState.counterWarriors[gameState.mapCells[1, 1].CounterWarrior];
            Assert.IsTrue(gameState.mapCells[1, 1].Warrior is Defender);
        }
    }
}
