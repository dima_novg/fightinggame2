﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCheckers.Models
{
    internal class SwordsMan : IWarrior
    {
        public Players Player { get; }
        public int Id => throw new NotImplementedException();

        public Color Color { get; }
        public string Name { get; set; }

        public int PhysicalDamage { get; }

        public int MagicDamage { get; }

        public int ShootingDamage  { get; }

        public int PhysicalArmor  { get; }

        public int MagicArmor  { get; }

        public int ShootingArmor { get; }

        public int XP { get; set; }

        

        public SwordsMan(GameState gameState)
        {
            Color = Color.Red;
            PhysicalArmor = 1;
            PhysicalDamage = 1;
            MagicArmor = 1;
            MagicDamage = 1;
            ShootingArmor = 1;
            ShootingArmor = 1;
            XP = 1;
            Player = gameState.activePlayer;
        }
        public FightStates Fight(IWarrior oppenent)
        {
            if (oppenent == null)
                return FightStates.Win;
            if (oppenent.Player == this.Player)
                return FightStates.Draw;
            if (oppenent is SwordsMan)
                return FightStates.Draw;
            if (oppenent is Archer)
                return FightStates.Death;
            if (oppenent is Defender)
                return FightStates.Win;
            throw new Exception();
        }

        public int GetSummaryArmor()
        {
            throw new NotImplementedException();
        }

        public int GetSummaryDamage()
        {
            throw new NotImplementedException();
        }

        public Image GetImage()
        {
            switch (this.Player)
            {
                case Players.FirstPlayer:
                    return Icons.FirstPlayerSwordsMan;
                case Players.SecondPlayer:
                    return Icons.SecondPlayerSwordsMan;
                default:
                    throw new Exception("ТЫ КТО СУКА");
            }
        }
    }
}
