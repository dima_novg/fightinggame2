﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoCheckers.Models;

namespace AutoCheckers
{
    public class FormGame : Form
    {
        GameState gameState = new GameState();
        Panel bigPanelBox = new Panel();
        public FormGame()
        {
            InitializeComponent();
            gameState.InitCounter();
            gameState.mapCells = new MapCell[8, 8];
            //GameState.activePlayer = Players.FirstPlayer;
            var menuStrip = new MenuStrip();
            Controls.Add(menuStrip);
            var aboutItem = new ToolStripMenuItem("Начать игру");
            var playerFirst = new ToolStripMenuItem("Первый игрок");
            var playerSecond = new ToolStripMenuItem("Второй игрок");
            menuStrip.Items.Add(aboutItem);
            menuStrip.Items.Add(playerFirst);
            menuStrip.Items.Add(playerSecond);
            bigPanelBox.Size = ClientSize;
            bigPanelBox.Location = new Point(0, menuStrip.Size.Height);        
            Controls.Add(bigPanelBox);
            for (var i = 0;  i < gameState.mapCells.GetLongLength(0); i++)
                for (var j = 0; j < gameState.mapCells.GetLongLength(1); j++)
                {
                    gameState.mapCells[i,j] = new MapCell(new Point(i,j));
                    var image = Icons.FirstPlayerArcher;
                    gameState.mapCells[i, j].Picture = new PictureBox()
                    {
                        Width = ClientSize.Width / 8, 
                        Height = ClientSize.Height / 8, 
                        BackColor = Color.AntiqueWhite,
                        BorderStyle = BorderStyle.FixedSingle,
                        Location = new Point(bigPanelBox.Width / 8 * i, bigPanelBox.Height / 8 * j)
                    };
                    bigPanelBox.Controls.Add(gameState.mapCells[i, j].Picture);
                    var i1 = i;
                    var j1 = j;
                    gameState.mapCells[i, j].Picture.Click += (sender, args) =>
                    {
                        gameState.mapCells[i1, j1].UpdateCounter();
                        gameState.mapCells[i1, j1].Warrior = gameState.counterWarriors[gameState.mapCells[i1, j1].CounterWarrior];                        
                        gameState.mapCells[i1, j1].Picture.Image = gameState.mapCells[i1, j1].Warrior == null ? null : gameState.mapCells[i1, j1].Warrior.GetImage();
                    };
                }
          
            aboutItem.Click += StartGame_Click;
            playerFirst.Click += (sender, args) =>
            {
                gameState.activePlayer = Players.FirstPlayer;
                gameState.InitCounter();
            };
            playerSecond.Click += (sender, args) =>
            {
                gameState.activePlayer = Players.SecondPlayer;
                gameState.InitCounter();
            };
        }
        private void StartGame_Click(object sender, EventArgs e)
        {
            while (!gameState.CheckEndGame())
            {
                gameState.TickBattle();
                bigPanelBox.Refresh();
                Thread.Sleep(200);
            }
        }


        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(800, 800);
            this.Cursor = System.Windows.Forms.Cursors.PanNW;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormGame";
            this.Text = "FormGame";
            this.ResumeLayout(false);

        }
    }
}
