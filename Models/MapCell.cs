﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCheckers.Models
{
    class MapCell : ICloneable
    {
        public IWarrior Warrior { get; set; }
        public int CounterWarrior { get; set; }
        public MapStates CellType;
        public PictureBox Picture { get; set;}
        private Point Point;

        public MapCell(Point point)
        {
            Point = point;
            CounterWarrior = 0;
        }

        public void UpdateCounter()
        {
            if (CounterWarrior < 3)
                CounterWarrior++;
            else
                CounterWarrior = 0;
        }

        public object Clone()
        {
            return new MapCell(this.Point) { Warrior = this.Warrior, CounterWarrior = this.CounterWarrior, Picture = this.Picture};
        }

        static public MapCell[,] CloneMap(MapCell[,] map) 
        {
            var newmap = new MapCell[map.GetLength(0), map.GetLength(1)];
            for (int i = 0; i < map.GetLength(0); i++) 
            {
                for (int j = 0; j < map.GetLength(1); j++) 
                {
                    newmap[i, j] = (MapCell)map[i, j].Clone();
                }
            }
            return newmap;
        }
    }
}
