﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AutoCheckers
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            var picture = new PictureBox
            {
                Image = new Bitmap(@"../../image/getImage.jpg"),
                Location = new Point(40,80),
                AutoSize = true,
                Size = new Size(300, 300)
            };
            Controls.Add(picture);
            var button = new Button()
            {
                Text = "Начать игру",
                Location = new Point(360, 500)
            };
            Controls.Add(button);
            var formGame = new FormGame();
            button.Click += (sender, e) =>
            {
                Hide();
                formGame.Show();
            };
            formGame.Closed += (sender, e) =>
            {
                this.Close();

            };

        }
    }
}
