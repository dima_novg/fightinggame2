﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AutoCheckers.Models;
using Point = System.Drawing.Point;

namespace AutoCheckers
{
    internal class GameState
    {
        public Players activePlayer{ get; set; } 
        public Dictionary<int, IWarrior> counterWarriors{ get; set; }
        public Dictionary<int, Point> moveVectorsUp { get; set; }
        public Dictionary<int, Point> moveVectorsDown { get; set; }
        public MapCell[,] mapCells { get; set;}

        public void InitCounter()
        {
            counterWarriors = new Dictionary<int, IWarrior>
            {
                {0, null}, 
                {1, new Archer(this)}, 
                {2, new Defender(this)}, 
                {3, new SwordsMan(this)}
            };

            moveVectorsUp = new Dictionary<int, Point>
            {
                {0, new Point(0,-1)},
                {1, new Point(-1,-1)},
                {2, new Point(1,-1)}
            };

            moveVectorsDown = new Dictionary<int, Point>
            {
                {0, new Point(0,1)},
                {1, new Point(1,1)},
                {2, new Point(-1,1)}
            };
        }

        public MapCell[,] TickBattle()
        {
            var rnd = new Random();
            var newMapCells = MapCell.CloneMap(mapCells);

            for (int i = 0; i < 8; i++) 
            {
                for (int j = 0; j < 8; j++) 
                {
                    if (mapCells[i, j].Warrior != null) 
                    {
                        newMapCells = Move(newMapCells, i, j, rnd);
                    }
                }
            }
            mapCells = newMapCells;
            return newMapCells;
        }
        //j=x i=y
        private MapCell[,] Move(MapCell[,] newMapCells, int i, int j, Random rnd)
        {
            var randomMove = rnd.Next(3);
            var moveVectors = mapCells[i, j].Warrior.Player == Players.FirstPlayer ? moveVectorsUp : moveVectorsDown;

            if (i + moveVectors[randomMove].X >= 8 || j + moveVectors[randomMove].Y >= 8 ||
                i + moveVectors[randomMove].X < 0 || j + moveVectors[randomMove].Y < 0
                )
            {
                return newMapCells;
            }

            var fightResult = mapCells[i, j].Warrior.Fight(newMapCells[i + moveVectors[randomMove].X, j + moveVectors[randomMove].Y].Warrior);

            switch (fightResult) 
            {
                case FightStates.Win:
                    {
                        newMapCells[i + moveVectors[randomMove].X, j + moveVectors[randomMove].Y].Warrior = mapCells[i, j].Warrior;
                        newMapCells[i + moveVectors[randomMove].X, j + moveVectors[randomMove].Y].Picture.Image = mapCells[i, j].Warrior.GetImage();
                        newMapCells[i, j].Warrior = null;
                        newMapCells[i, j].Picture.Image = null;
                        return newMapCells;
                    }
                case FightStates.Death:
                    {
                        newMapCells[i, j].Warrior = null;
                        newMapCells[i, j].Picture.Image = null;
                        return newMapCells;
                    }
                default:
                    return newMapCells;
            }
        }

        public bool CheckEndGame()
        {
            var gameEnds = true;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (mapCells[i, j].Warrior != null)
                    {
                        if (CanMove(i, j))
                            gameEnds = false;
                    }
                }
            }
            return gameEnds;
        }

        private bool CanMove(int i, int j) 
        {
            var moveVectors = mapCells[i, j].Warrior.Player == Players.FirstPlayer ? moveVectorsUp : moveVectorsDown;
            var answer = false;
            foreach (var e in moveVectors) 
            {
                if (i + e.Value.X >= 8 || j + e.Value.Y >= 8 || i + e.Value.X < 0 || j + e.Value.Y < 0)
                    continue;                
                var fightResult = mapCells[i, j].Warrior.Fight(mapCells[i + e.Value.X, j + e.Value.Y].Warrior);
                if (fightResult != FightStates.Draw) 
                {
                    answer = true;
                    break;
                }
            }
            return answer;
        }
    }
}
