﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCheckers.Models
{
    
    interface IWarrior
    {
        Players Player { get; }
        int Id { get; }
        string Name { get; set; }
        Color Color { get; }
        int PhysicalDamage { get; }
        int MagicDamage { get; }
        int ShootingDamage { get; }

        int PhysicalArmor { get; }
        int MagicArmor { get;}
        int ShootingArmor { get; }

        int XP { get; }

        int GetSummaryDamage();

        int GetSummaryArmor();

        Image GetImage();

        FightStates Fight(IWarrior oppenent);

    }
}
