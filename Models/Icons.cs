﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCheckers.Models
{
    public static class Icons
    {
        public static Image FirstPlayerArcher = new Bitmap(@"../../image/BlueArcher.png");
        public static Image SecondPlayerArcher = new Bitmap(@"../../image/RedArcher.png");      
        public static Image FirstPlayerDefender  = new Bitmap(@"../../image/BlueShield.png");
        public static Image SecondPlayerDefender = new Bitmap(@"../../image/RedShiled.png");
        public static Image FirstPlayerSwordsMan = new Bitmap(@"../../image/BlueSwordsman.png");
        public static Image SecondPlayerSwordsMan = new Bitmap(@"../../image/RedSwordsman.png");
        public static Image Mini = new Bitmap(@"../../image/mini.png");

    }
}
